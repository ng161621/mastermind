# MasterMind

Scenario 1 : A player chooses a combination of length 4, 2 well placed colors and 1 misplaced but correct color and 1 non-existent color in the secret combination.

**Given** the secret combination

|<span style="color:red">Red</span>|<span style="color:black">Black</span>|<span style="color:green">Green</span>|<span style="color:green">Green</span>|
|---|---|---|---|

**When** the player gives the combination

|<span style="color:red">Red</span>|<span style="color:blue">Blue</span>|<span style="color:black">Black</span>|<span style="color:green">Green</span>|
|---|---|---|---|

**Then** the algorithm return

Legend :

|<span style="color:red">Red</span>|Wrong color|
|---|---|

|<span style="color:orange">Orange</span>|Correct but badly placed|
|---|---|

|<span style="color:green">Green</span>|Correct and well placed|
|---|---|

---

**Return**
|<span style="color:red">Red</span>|<span style="color:blue">Blue</span>|<span style="color:black">Black</span>|<span style="color:green">Green</span>|
|---|---|---|---|

Nicolas GARNIER | ESGI AL2